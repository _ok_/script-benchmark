#!/usr/bin/env python3
import sys

_, prefix = sys.argv
for line in sys.stdin:
    sys.stdout.write(prefix + line)

