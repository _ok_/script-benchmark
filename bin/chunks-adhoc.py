#!/usr/bin/env python3
import sys
import os
from functools import reduce, partial

BUF_SIZE = int(os.environ.get("BUFFER_SIZE", 32 * 1024))


def chunks(read, length=BUF_SIZE):
    while True:
        chunk = read(length)
        if not chunk:
            break
        yield chunk


def process_chunk(leftover, chunk, write, prefix):
    buf, nl, rest = chunk.rpartition(b"\n")
    write(leftover)  # write prefix if it was returned last time
    write(buf.replace(b"\n", b"\n" + prefix))
    write(nl)
    if nl and rest:  # write prefix in between \n and rest if both are nonempty
        write(prefix)
    write(rest)
    return b"" if rest else prefix  # to be written next time, unless it's EOF


_, prefix = sys.argv
reduce(
    partial(process_chunk, write=sys.stdout.buffer.write, prefix=prefix.encode()),
    chunks(sys.stdin.buffer.read),
    prefix.encode(),
)

