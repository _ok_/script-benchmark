#!/usr/bin/env lua
prefix = arg[1]
while true do
	local line = io.read()
	if line == nil then break end
	io.write(prefix, line, '\n')
end
