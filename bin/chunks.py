#!/usr/bin/env python3
import sys
import os
from functools import reduce, partial

BUF_SIZE = int(os.environ.get("BUFFER_SIZE", 32 * 1024))
NL = b"\n"


def process_chunk(func, accum, raw):
    buf, nl, rest = raw.rpartition(NL)
    if nl:
        accum.append(buf)
        func(accum, nl)
        accum = []
    return accum + [rest] if rest else accum


def nl_chunks(chunks):
    """Reorganize chunks, so each ends in newline (except maybe the last one).
    Output chunks are split into parts, because it's cheaper to write multiple parts
    than to join them into string"""
    accum = []
    for chunk in chunks:
        buf, nl, rest = chunk.rpartition(NL)
        if nl:
            yield (accum + [buf], nl)
            accum = []
        if rest:
            accum.append(rest)
    if accum:
        yield (accum, b"")


def prepend(parts, end, write, prefix):
    """Prepend chunk lines with prefix and write"""
    write(prefix)
    for part in parts:  # we only have \n in the last part, but this way is simpler
        write(part.replace(NL, NL + prefix))
    write(end)


def chunks(read, length=BUF_SIZE):
    while True:
        chunk = read(length)
        if not chunk:
            return
        yield chunk


_, prefix = sys.argv
prefix = prefix.encode()


for chunk in nl_chunks(chunks(sys.stdin.buffer.read)):
    prepend(*chunk, write=sys.stdout.buffer.write, prefix=prefix)

