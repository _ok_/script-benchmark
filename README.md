# Scripting benchmark

Simple task implemented in several scripting languages.
The task is to append a string (provided as CLI arg) to each line from stdin.

# How to run

Generate `input` file, e.g.:

```
seq -w 1000000 | perl -lne '$,=","; print $_, $., 1/$.' > input
```

Inspect `./run` script for options. `${REPEAT:-3}` means that `3` will be used
unless you set `REPEAT` variable, e.g. with `REPEAT=10 ./run`.

Install all necessary binaries (mawk, python3, pypy3, perl, lua), although if
some of them are missing, they won't break the benchmark. `mawk` is used for
reference files, although it can be configured with `GOLDEN_CMD`

With Nix, you can run

```
nix-shell -p pypy3 php luajit mawk dash
```

Run the benchmark for the scripts which interest you:

```
./run bin/*
```

Shells are really slow, so I put the script separately from others. It'll be
ran against `bash` and `dash`:

```
./run simple.sh
```

# Results

Prepending `1234567890====,` string to each of 3M lines of ~100MB input file
located in `/dev/shm/` on NixOS:

```
$ PRECISION=2 REPEAT=30 PYPY=1 ./run bin/* 2> >(sort -n)
real	user	system	command
3.48	2.74	0.73	python3 bin/chunks-adhoc.py
3.61	2.91	0.69	python3 bin/chunks.py
9.81	8.66	1.15	bin/mawk
10.30	8.86	1.43	pypy3 bin/chunks-adhoc.py
10.42	8.99	1.43	pypy3 bin/chunks.py
11.78	10.72	1.06	bin/sed
17.59	16.58	1.00	bin/awk
18.91	17.32	1.59	pypy3 bin/simple.py
20.28	19.50	0.78	bin/perl
27.69	26.60	1.09	bin/simple.lua
30.21	29.25	0.94	python3 bin/simple.py
49.63	48.76	0.86	bin/perl-sub
62.50	43.31	19.18	bin/simple.php
```

Shells with `REPEAT=3` (multiply by 10 to compare with above):

```
243.88	128.89	114.96	dash simple.sh
335.63	322.41	13.17	bash simple.sh
```

# Discussion

In general, `mawk` is the easiest tool to write performant scripts. It's
significatly faster than GNU `awk`.

Next, for regexes there is often a choice between `sed` and `perl`. In my
experience, `perl` is faster when you need capture groups or `/g`. In this
case, `sed` is a lot faster than substitution in Perl, and also faster than
string concatenation in Perl. OTOH, `sed` doesn't allow passing arguments, so
`sed "s/^/$1/"` is quite error-prone.

PHP (version 8, same holds for 7) seems ridiculously slow, maybe there are some
obvious optimizations I missed.

`simple.py` is pretty slow as expected, and PyPy helps a bit. But what
motivated this whole benchmark in the first place is that it's not really hard
to optimize Python script to process input in chunks. `chunks-adhoc.py` is
quite straightforward implementation, but not very generalizable for similar
tasks. OTOH, `chunks.py` has functions which could be reused in similar
scripts, and is only marginally slower. Also interestingly, CPython performs
better than PyPy for these scripts.

It should be straightforward to modify Lua, Perl & PHP scripts to work with
chunks, but because I'm a noob in those languages, there is really no point.
GNU parallel (written in Perl) for sure can chunk its input, but I didn't find
a way to make it run Perl functions instead of launching subprocesses.
